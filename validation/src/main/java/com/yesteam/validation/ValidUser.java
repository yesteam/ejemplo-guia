/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;

/**
 * Esta es la anotación que usaremos en los elementos que queramos validar.
 * 
 * En este caso es una anotación aplicable a strings, que aunque solo usaremos para anotar un string pasado
 * por argumento, podría usarse para un atributo o el resultado de un método
 */
@Documented
@Constraint(validatedBy = ValidUserValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@NotNull //a parte de chequear que el usuario exista, chequeamos que el texto no sea null
public @interface ValidUser {

    /**
     * Este es el mensaje que se imprimirá al fallar la validación.
     * 
     * Si el mensaje está entre llaves ({}), entonces se leerá del fichero de propiedades ValidationMessages.properties.
     * Este fichero tiene que estar en la raiz (es decir, sin estar contenido por ningún paquete). En un proyecto
     * normal podría estár junto a las fuentes java, pero en Maven, por defecto, la carpeta /src/main/java es filtrada y no
     * se recogen ficheros no fuente. Por eso hay que guardarlo en /src/main/resources/.
     * 
     * Usar este fichero ValidationMessages.properties permite internacionalizar fácilmente la aplicacion.
     * @return 
     */
    String message() default "{com.yesteam.validation.ValidUser}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
