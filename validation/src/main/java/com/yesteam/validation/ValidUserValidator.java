/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.validation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Esta clase es la que contiene la lógica de validación de la anotación {@link ValidUser}.
 */
public class ValidUserValidator implements ConstraintValidator<ValidUser, String> {
    
    private static final Set<String> registredUsers;
    
    static {
        Set<String> temp = new HashSet<>(3);
        temp.add("paco");
        temp.add("mattia");
        temp.add("gonzalo");
        
        registredUsers = Collections.unmodifiableSet(temp);
    }
    
    @Override
    public void initialize(ValidUser constraintAnnotation) {
    }
    
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return registredUsers.contains(value);
    }
}
