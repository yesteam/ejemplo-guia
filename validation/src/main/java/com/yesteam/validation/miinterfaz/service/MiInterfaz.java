/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.validation.miinterfaz.service;

import com.yesteam.validation.ValidUser;

/**
 * La interfaz sencilla.
 */
public interface MiInterfaz {

    /**
     * Lamentablemente, no he conseguido que la anotación de validación se pase por defecto a la clase que lo
     * implementa. Sin embargo es importante ponerlo en la interfaz para que quien la use sepa, mirando el
     * javadoc, qué precondiciones y postcondiciones tiene.
     * @param myName
     * @return 
     */
    String getMessage(@ValidUser String myName);
}
