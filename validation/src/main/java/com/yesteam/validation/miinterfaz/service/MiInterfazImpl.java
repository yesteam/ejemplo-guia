/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.validation.miinterfaz.service;

import com.yesteam.validation.ValidUser;
import javax.enterprise.context.Dependent;

/**
 * Esta es la clase que implementa este servicio sencillo de prueba
 */
@Dependent //el scope es necesario para que CDI lo vea como un bean injectable
public class MiInterfazImpl implements MiInterfaz {

    /**
     * Lamentablemente no he encontrado la manera de que la constraint (@ValidUser) se herede desde la interfaz,
     * así que hay que ponerla también en la clase hija. Si no se pone, Bean validator no va a añadir los
     * chequeos!
     * @param myName
     * @return 
     */
    @Override
    public String getMessage(@ValidUser String myName) {
        return "Hello " + myName;
    }

}
