/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.authorization.miinterfaz.service;

import javax.enterprise.context.Dependent;

/**
 * Esta es la clase que contiene la lógica de negocio de la interfaz.
 * 
 * Aquí se tiene que escribir el código asociado a la acción protegida suponiendo que ya se ha chequeado que
 * tenemos permiso para ejecutarla. De esta manera, si la política de seguridad cambia, esta clase nunca será
 * tocada.
 */
@Dependent
public class MiInterfazSensibleImpl implements MiInterfazSensible {

    @Override
    public String getMessage(String myName) {
        return "Hello " + myName;
    }

}
