/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yesteam.authorization.miinterfaz.service;

/**
 * Esta interfaz es "sensible" porque necesita autenticación.
 * <p/>
 * Nota: No hace falta usar el sufijo "sensible" en los recursos que sean sensibles!
 * 
 * Hay dos clases que la implementan:
 * <ul>
 * <li>{@link MiInterfazSensibleImpl} contiene la lógica de negocio sin la seguridad</li>
 * <li>{@link MiInterfazSensibleAuth} es un decorator que decora la lógica de negocio con la seguridad</li>
 * </ul>
 */
public interface MiInterfazSensible {

    String getMessage(String myName);
}
