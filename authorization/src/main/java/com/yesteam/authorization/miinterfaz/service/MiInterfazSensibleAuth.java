/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.authorization.miinterfaz.service;

import com.yesteam.authorization.YesSecurityException;
import com.yesteam.authorization.users.service.UserService;
import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import javax.interceptor.Interceptor;

/**
 * Esta es la implementación de la seguridad de la interfaz. Esta clase sigue el patron decorator y hace uso
 * de las facilidades para implementarlo que da JEE.
 * 
 * Si la seguridad fuese muy compleja, pueden crearse más delegators. Cada uno irá delegando en el otro en orden
 * inversamente proporcional al valor asignado en @Priority o en el xml correspondiente.
 * 
 * Nota: {@link Priority} fue incorporada en JEE 7. Si queremos usarlo en JEE 6, hay que usar xmls
 */
@Decorator
@Priority(Interceptor.Priority.APPLICATION + 10) //es importante mantener una prioridad correcta para no interferir con el contenedor
public class MiInterfazSensibleAuth implements MiInterfazSensible {

    /**
     * Esta es la clase en la que delegaremos. El contenedor JEE se encarga de instanciarla correctamente.
     * No podemos suponer que es una instancia de {@link MiInterfazSensibleImpl}, porque podría haber más
     * delegators entre medias.
     */
    @Inject
    @Delegate
    MiInterfazSensible delegate;

    /**
     * Aquí injecto este otro servicio. Esto es algo totalmente dependiente de este ejemplo, no siempre tiene
     * que ser así. Sin embargo si la lógica de acceso es compleja o se repite en varios sitios, es bueno
     * centrarla en otro servicio
     */
    @Inject UserService userService;
    
    @Override
    public String getMessage(String myName) {
        if (authorizationConstraint(myName)) { //si esta autorizado
            return delegate.getMessage(myName); //entonces ejecutamos la lógica delegando en el delegator
        }
        else { //si no está autorizado
            throw new YesSecurityException(myName + " no tiene permiso para ejecutar esta acción"); //lanzamos una excepción
        }
    }
    
    /**
     * Este método no es necesario. Está aquí solo para hacer más legible el método {@link #getMessage(java.lang.String)}.
     * 
     * @param myName
     * @return 
     */
    private boolean authorizationConstraint(String myName) {
        return userService.isRegistred(myName);
    }
}
