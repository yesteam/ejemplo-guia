/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.authorization;

/**
 * La excepción que se lanza cuando no se tiene permiso. Aunque es recomendable tener una excepcion de seguridad
 * base de la cual hereden todas las demás, no tiene porqué llamarse así!
 */
public class YesSecurityException extends RuntimeException {

    public YesSecurityException() {
    }

    public YesSecurityException(String message) {
        super(message);
    }

    public YesSecurityException(String message, Throwable cause) {
        super(message, cause);
    }

    public YesSecurityException(Throwable cause) {
        super(cause);
    }

}
