/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.authorization.users.service;

import com.yesteam.authorization.miinterfaz.service.MiInterfazSensibleAuth;

/**
 * Es un servicio sencillo al que se pregunta si un string corresponde a un usuario registrado.
 * 
 * En el ejemplo, {@link MiInterfazSensibleAuth} delega en este servicio para saber si el usuario es válido o no
 */
public interface UserService {

    boolean isRegistred(String name);
}
