/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.yesteam.authorization.users.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.Dependent;

/**
 * Es una implementación sencilla del servicio de usuarios. Normalmente debería acceder a una base de datos y
 * demás. En este caso nos basta con tener una lista de usuarios válidos.
 */
@Dependent //añadiendo una anotación de "scope" nos aseguramos de que esta clase sea injectable. En este caso concreto el scope en si no es muy importante
public class UserServiceImpl implements UserService {

    private static final Set<String> registredUsers;
    
    static {
        Set<String> temp = new HashSet<>(3);
        temp.add("paco");
        temp.add("mattia");
        temp.add("gonzalo");
        
        registredUsers = Collections.unmodifiableSet(temp);
    }
    
    @Override
    public boolean isRegistred(String name) {
        return registredUsers.contains(name);
    }

}
